import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

//Components
import Layout from '@components/admin/Layout'

//Utils
import addClassColor from '@utils/classColor'

//actions
import {
  listProducts,
  createProduct,
  updateProduct,
  deleteProduct,
} from '../../store/actions/products'

function Inventory() {
  const dispatch = useDispatch()

  const { products } = useSelector((state) => state.products)

  const [name, setName] = useState('')
  const [price, setPrice] = useState('')
  const [category, setCategory] = useState('Empanadas')
  const [search, setSearch] = useState('')
  const [productId, setProductId] = useState('')

  let currentProducts = !search
    ? products
    : products.filter((p) => p.name.toLowerCase().includes(search))

  useEffect(() => {
    dispatch(listProducts())
  }, [])

  const setSelectedProduct = (product) => {
    setName(product.name)
    setPrice(product.price)
    setCategory(product.category)
    setProductId(product._id)
  }

  const removeInputs = () => {
    setName('')
    setPrice('')
    setProductId('')
    setCategory('Empanadas')
  }

  const handleCreateProduct = (e) => {
    if (confirm('Estas seguro de guardar el producto?')) {
      dispatch(
        createProduct({
          name,
          price,
          category,
        })
      )
      dispatch(listProducts())
      removeInputs()
    }
  }

  const handleUpdateProduct = (e) => {
    if (confirm('Estas seguro de actualizar el producto?')) {
      dispatch(
        updateProduct(productId, {
          name,
          price,
          category,
        })
      )
      dispatch(listProducts())
      removeInputs()
    }
  }

  const handleRemoveProduct = (id) => {
    if (confirm('Estas seguro de eliminar el producto?')) {
      dispatch(deleteProduct(id))
      dispatch(listProducts())
    }
  }

  return (
    <Layout title='Inventory' section={'Inventory'}>
      <div className='flex px-8 mt-5'>
        <div className='w-3/4'>
          <div className='pb-4'>
            <label htmlFor='table-search' className='sr-only'>
              Search
            </label>
            <div className='relative mt-1'>
              <div className='flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none'>
                <svg
                  className='w-5 h-5 text-gray-500 dark:text-gray-400'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path
                    fillRule='evenodd'
                    d='M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z'
                    clipRule='evenodd'
                  ></path>
                </svg>
              </div>
              <input
                type='text'
                id='table-search'
                className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-80 pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                placeholder='Search for items'
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
          </div>
          <div className='flex flex-wrap'>
            {currentProducts
              ?.sort((a, b) => a.price - b.price)
              .map((product) => {
                let color = addClassColor(Number(product.price))
                return (
                  <div
                    key={product._id}
                    className={`px-8 py-3 mb-5 mr-3 rounded-md cursor-pointer relative ${color}`}
                    onClick={() => setSelectedProduct(product)}
                  >
                    {product.name}
                    <div
                      onClick={() => handleRemoveProduct(product._id)}
                      className='absolute -top-2 bg-slate-100 w-6 h-6 text-xs rounded-full right-0 grid place-items-center cursor-pointer border-2'
                    >
                      x
                    </div>
                  </div>
                )
              })}
          </div>
        </div>
        <div className='w-1/4 shadow-lg p-3 ml-3'>
          <h3 className='text-center text-2xl mb-5'>
            {productId ? 'Update' : 'Create'} Product
          </h3>
          <form>
            <div className='flex flex-col mb-2'>
              <label
                htmlFor='name'
                className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400'
              >
                Nombre
              </label>
              <input
                id='name'
                className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                onChange={(e) => setName(e.target.value)}
                value={name}
              />
            </div>

            <div className='flex flex-col mb-2'>
              <label
                htmlFor='price'
                className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400'
              >
                Precio
              </label>
              <input
                id='price'
                className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5'
                onChange={(e) => setPrice(e.target.value)}
                value={price}
              />
            </div>

            <div className='flex flex-col mb-5'>
              <label
                htmlFor='categories'
                className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400'
              >
                Categoria
              </label>
              <select
                id='categories'
                className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                onChange={(e) => setCategory(e.target.value)}
                value={category}
              >
                <option defaultChecked>{category}</option>
                <option value='empanadas'>Empanadas</option>
                <option value='bebidas'>Bebidas</option>
                <option value='dulces'>Dulces</option>
              </select>
            </div>

            <div className='grid grid-cols-2 gap-x-5'>
              <button
                type='button'
                onClick={removeInputs}
                className='py-4 rounded-xl bg-red-400 hover:bg-red-500'
              >
                Cancelar
              </button>
              <button
                type='submit'
                className='py-4 rounded-xl bg-green-400 hover:bg-green-500'
                onClick={!productId ? handleCreateProduct : handleUpdateProduct}
              >
                {productId ? 'Actualizar' : 'Guardar'}
              </button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  )
}

export default Inventory
