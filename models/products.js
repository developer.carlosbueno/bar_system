import mongoose, { Schema, model } from 'mongoose'

const productSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
})

const Product = mongoose.models.products || model('products', productSchema)

export default Product
