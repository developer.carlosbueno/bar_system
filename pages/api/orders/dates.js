import nc from 'next-connect'
import Orders from '@models/orders'
import Cuadre from '@models/cuadre'
import { success, fail } from '@utils/response'
import db from '@utils/db'
import { protect } from '../../../middlewares/authMiddlewares'

const handler = nc()

// handler.use(protect)

handler.post(async (req, res) => {
  const { from, to } = req.body

  try {
    await db.connect()
    const orders = await Orders.find({
      createdAt: {
        $gte: new Date(from),
        $lte: new Date(to),
      },
    })
    await db.disconnect()
    success(res, 200, orders)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
