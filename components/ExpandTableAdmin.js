import * as React from 'react'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import IconButton from '@mui/material/IconButton'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Typography from '@mui/material/Typography'
import Paper from '@mui/material/Paper'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'

import formatDate from '@utils/formatDate'
import { Button } from '@mui/material'

function createData(name, total, date, totalPrice, history) {
  return {
    name,
    total,
    date,
    totalPrice,
    history,
  }
}

function Row(props) {
  const { row, handlePrintCuadre } = props
  const [open, setOpen] = React.useState(false)

  const cash = row.history
    .filter((i) => i.paymentMethod.includes('Efectivo'))
    .reduce((acc, c) => acc + c.totalPrice, 0)

  const card = row.history
    .filter((i) => i.paymentMethod.includes('Tarjeta'))
    .reduce((acc, c) => acc + c.totalPrice, 0)

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label='expand row'
            size='small'
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component='th' scope='row'>
          {row.name}
        </TableCell>
        <TableCell component='th' scope='row'>
          {row.total}
        </TableCell>
        <TableCell component='th' scope='row'>
          {formatDate(new Date(row.date), 'year')}
        </TableCell>
        <TableCell align='right'>
          ${Number(row.totalPrice).toFixed(2)}
        </TableCell>
        <TableCell align='right'>
          <Button
            variant='contained'
            onClick={() => handlePrintCuadre(row.history, cash, card)}
          >
            Ok
          </Button>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant='h6' gutterBottom component='div'>
                Facturas
              </Typography>
              <Table size='small' aria-label='purchases'>
                <TableHead>
                  <TableRow>
                    <TableCell>Articulos</TableCell>
                    <TableCell align='right'>Metodo de pago</TableCell>
                    <TableCell align='right'>Monto($)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.history?.map((historyRow) => (
                    <TableRow key={historyRow._id}>
                      <TableCell component='th' scope='row'>
                        {historyRow?.orderItems?.reduce(
                          (acc, current) => acc + current.qty,
                          0
                        )}
                      </TableCell>
                      <TableCell align='right'>
                        {historyRow.paymentMethod}
                      </TableCell>
                      <TableCell align='right'>
                        {historyRow.totalPrice}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  )
}

export default function CollapsibleTable({ info, handlePrintCuadre }) {
  const rows = []

  info?.forEach((i) => {
    let history = i.orders
    let totalPrice = history.reduce((acc, i) => acc + i.totalPrice, 0)
    if (history.length !== 0) {
      rows.push(
        createData(
          i.user.name,
          history.length,
          i.createdAt,
          totalPrice,
          history
        )
      )
    }
  })

  return (
    <TableContainer component={Paper}>
      <Table aria-label='collapsible table'>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Name</TableCell>
            <TableCell>Total Facturas</TableCell>
            <TableCell>Fecha</TableCell>
            <TableCell align='right'>TOTAL($)</TableCell>
            <TableCell align='right'>Imprimir</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row
              key={row.name}
              row={row}
              handlePrintCuadre={handlePrintCuadre}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
