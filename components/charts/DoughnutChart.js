import React, { useState, useEffect } from 'react'
import {
  Chart as ChartJS,
  BarElement,
  CategoryScale,
  LinearScale,
  ArcElement,
} from 'chart.js'

import { Doughnut } from 'react-chartjs-2'

ChartJS.register(BarElement, CategoryScale, LinearScale, ArcElement)

const BarChart = () => {
  var data = {
    labels: ['Red', 'Blue', 'Yellow'],
    datasets: [
      {
        label: 'My First Dataset',
        data: [300, 50, 100],
        backgroundColor: [
          'rgb(255, 99, 132)',
          'rgb(54, 162, 235)',
          'rgb(255, 205, 86)',
        ],
        hoverOffset: 4,
      },
    ],
  }

  var options = {
    maintainAspectRatio: false,
    scales: {},
    legend: {
      labels: {
        fontSize: 25,
      },
    },
  }

  return (
    <div>
      <Doughnut data={data} height={400} width={500} options={options} />
    </div>
  )
}

export default BarChart
