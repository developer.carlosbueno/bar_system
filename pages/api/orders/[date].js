import nc from 'next-connect'
import Orders from '@models/orders'
import Cuadre from '@models/cuadre'
import { success, fail } from '@utils/response'
import db from '@utils/db'
// import { protect } from '../../../middlewares/authMiddlewares'

const handler = nc()

// handler.use(protect)

handler.get(async (req, res) => {
  const { date } = req.query
  try {
    await db.connect()
    const orders = await Orders.find({
      createdAt: {
        $gte: new Date(date),
      },
    })
      .populate('user')
      .sort({ createdAt: -1 })
    await db.disconnect()
    success(res, 200, orders)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
