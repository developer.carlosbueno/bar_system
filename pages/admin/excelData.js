import { useRouter } from 'next/router'

//Components
import Layout from '@components/admin/Layout'

function ExcelData() {
  const router = useRouter()
  const data = router.query.orders && JSON.parse(router.query.orders)

  return (
    <Layout title={'Exportar excel'}>
      <div className='relative overflow-x-auto shadow-md sm:rounded-lg mt-5'>
        <table className='w-full text-sm text-left text-gray-500 dark:text-gray-400'>
          <thead className='text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400'>
            <tr>
              <th scope='col' className='px-6 py-3'>
                Product name
              </th>
              <th scope='col' className='px-6 py-3'>
                Cantidad
              </th>
            </tr>
          </thead>
          <tbody>
            {data?.map((d) => (
              <tr
                key={d.name}
                className='border-b dark:bg-gray-800 dark:border-gray-700 odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700'
              >
                <th
                  scope='row'
                  className='px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap'
                >
                  {d.name}
                </th>
                <td className='px-6 py-4'>{d.qty}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Layout>
  )
}

export default ExcelData
