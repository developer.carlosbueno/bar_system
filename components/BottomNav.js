import { GiHamburgerMenu } from 'react-icons/gi'
import { FaCashRegister } from 'react-icons/fa'
import { RiNewspaperFill } from 'react-icons/ri'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

function BottomNav({ setOpen, open }) {
  const router = useRouter()
  const [active, setActive] = useState(true)

  useEffect(() => {
    if (
      router.pathname.includes('print') ||
      router.pathname.includes('admin')
    ) {
      setActive(false)
    } else {
      setActive(true)
    }
  }, [router.pathname])

  return (
    <div className={`px-20 h-20 bottomnav ${active ? '' : 'hidden'}`}>
      <Link href={'/cuadre'} passHref>
        <FaCashRegister />
      </Link>
      <Link href={'/facturation'}>
        <RiNewspaperFill className='mx-20' />
      </Link>
      <GiHamburgerMenu onClick={() => setOpen(!open)} />
    </div>
  )
}

export default BottomNav
