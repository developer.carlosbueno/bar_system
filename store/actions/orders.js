import axios from 'axios'

export const actions = {
  ORDER_CREATE_REQUEST: 'ORDER_CREATE_REQUEST',
  ORDER_CREATE_SUCCESS: 'ORDER_CREATE_SUCCESS',
  ORDER_CREATE_FAIL: 'ORDER_CREATE_FAIL',
  ORDER_DETAILS_REQUEST: 'ORDER_DETAILS_REQUEST',
  ORDER_DETAILS_SUCCESS: 'ORDER_DETAILS_SUCCESS',
  ORDER_DETAILS_FAIL: 'ORDER_DETAILS_FAIL',
  ORDER_MY_LIST_REQUEST: 'ORDER_MY_LIST_REQUEST',
  ORDER_MY_LIST_SUCCESS: 'ORDER_MY_LIST_SUCCESS',
  ORDER_MY_LIST_FAIL: 'ORDER_MY_LIST_FAIL',
  ORDER_ALL_REQUEST: 'ORDER_ALL_REQUEST',
  ORDER_ALL_SUCCESS: 'ORDER_ALL_SUCCESS',
  ORDER_ALL_FAIL: 'ORDER_ALL_FAIL',
  ORDER_TODAY_REQUEST: 'ORDER_TODAY_REQUEST',
  ORDER_TODAY_SUCCESS: 'ORDER_TODAY_SUCCESS',
  ORDER_TODAY_FAIL: 'ORDER_TODAY_FAIL',
  ORDER_MONTH_REQUEST: 'ORDER_MONTH_REQUEST',
  ORDER_MONTH_SUCCESS: 'ORDER_MONTH_SUCCESS',
  ORDER_MONTH_FAIL: 'ORDER_MONTH_FAIL',
  ORDER_DATES_REQUEST: 'ORDER_DATES_REQUEST',
  ORDER_DATES_SUCCESS: 'ORDER_DATES_SUCCESS',
  ORDER_DATES_FAIL: 'ORDER_DATES_FAIL',
  ORDER_DELIVER_REQUEST: 'ORDER_DELIVER_REQUEST',
  ORDER_DELIVER_SUCCESS: 'ORDER_DELIVER_SUCCESS',
  ORDER_DELIVER_FAIL: 'ORDER_DELIVER_FAIL',
  ORDER_DELIVER_RESET: 'ORDER_DELIVER_RESET',
  ORDER_PROFITS_REQUEST: 'ORDER_PROFITS_REQUEST',
  ORDER_PROFITS_SUCCESS: 'ORDER_PROFITS_SUCCESS',
  ORDER_PROFITS_FAIL: 'ORDER_PROFITS_FAIL',
  ORDER_RESET: 'ORDER_RESET',
}

const getDate = () => {
  var now = new Date()
  var startOfToday = new Date(now.getFullYear(), now.getMonth(), now.getDate())
  return startOfToday
}

export const createOrder = (info) => async (dispatch, getState) => {
  dispatch({ type: actions.ORDER_CREATE_REQUEST })

  const {
    user: { userInfo },
  } = getState()

  let config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${userInfo.token}`,
    },
  }

  try {
    const { data } = await axios.post('/api/orders', info, config)
    dispatch({ type: actions.ORDER_CREATE_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.ORDER_CREATE_FAIL, payload: error.message })
  }
}

export const listMyOrders = () => async (dispatch, getState) => {
  dispatch({ type: actions.ORDER_MY_LIST_REQUEST })

  const {
    user: { userInfo },
  } = getState()

  let config = {
    headers: {
      Authorization: `Bearer ${userInfo.token}`,
    },
  }

  try {
    const { data } = await axios.get(`/api/orders/${userInfo._id}`, config)
    dispatch({ type: actions.ORDER_MY_LIST_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.ORDER_MY_LIST_FAIL, payload: error.message })
  }
}

export const listOrders = () => async (dispatch, getState) => {
  dispatch({ type: actions.ORDER_ALL_REQUEST })

  const {
    user: { userInfo },
  } = getState()

  let config = {
    headers: {
      Authorization: `Bearer ${userInfo.token}`,
    },
  }

  try {
    const { data } = await axios.get(`/api/orders`, config)
    dispatch({ type: actions.ORDER_ALL_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.ORDER_ALL_FAIL, payload: error.message })
  }
}

export const listTodayOrders = () => async (dispatch, getState) => {
  dispatch({ type: actions.ORDER_TODAY_REQUEST })

  const {
    user: { userInfo },
  } = getState()

  let config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${userInfo.token}`,
    },
  }
  try {
    const { data } = await axios.get(`/api/orders/${getDate()}`, config)
    dispatch({ type: actions.ORDER_TODAY_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.ORDER_TODAY_FAIL, payload: error.message })
  }
}

export const listMonthOrders = () => async (dispatch, getState) => {
  dispatch({ type: actions.ORDER_MONTH_REQUEST })

  const {
    user: { userInfo },
  } = getState()

  let config = {
    headers: {
      Authorization: `Bearer ${userInfo.token}`,
    },
  }

  try {
    const { data } = await axios.get(`/api/orders/${getDate()}`, config)
    dispatch({ type: actions.ORDER_MONTH_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.ORDER_MONTH_FAIL, payload: error.message })
  }
}

export const listDatesOrders = (from, to) => async (dispatch, getState) => {
  dispatch({ type: actions.ORDER_DATES_REQUEST })

  const {
    user: { userInfo },
  } = getState()

  let config = {
    headers: {
      Authorization: `Bearer ${userInfo.token}`,
    },
  }

  try {
    const { data } = await axios.post(`/api/orders/dates`, { from, to }, config)
    dispatch({ type: actions.ORDER_DATES_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.ORDER_DATES_FAIL, payload: error.message })
  }
}
