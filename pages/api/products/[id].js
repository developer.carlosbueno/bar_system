import nc from 'next-connect'
import Product from '@models/products'
import { success, fail } from '@utils/response'
import db from '@utils/db'

const handler = nc()

handler.put(async (req, res) => {
  const { name, price, category } = req.body

  try {
    await db.connect()
    const updatedProduct = await Product.findById(req.query.id)
    updatedProduct.name = name
    updatedProduct.price = price
    updatedProduct.category = category
    await updatedProduct.save()
    await db.disconnect()
    success(res, 200, updatedProduct)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

handler.delete(async (req, res) => {
  try {
    await db.connect()
    const deletedProduct = await Product.deleteOne({ _id: req.query.id })
    await db.disconnect()
    success(res, 200, deletedProduct)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
