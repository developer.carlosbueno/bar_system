const success = (res, status, body) => {
  res.status(status).json({
    error: false,
    body,
  })
}

const fail = (res, status, body) => {
  res.status(status).json({
    error: true,
    body,
  })
}
export { success, fail }
