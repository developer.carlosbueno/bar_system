import { Provider } from 'react-redux'
import { SnackbarProvider } from 'notistack'
import store from '../store'

import '../styles/globals.css'

//Components
import Layout from '../components/Layout'

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SnackbarProvider>
    </Provider>
  )
}

export default MyApp
