import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'

//Utils
import formatDate from '@utils/formatDate'

const PrintCuadre = () => {
  const router = useRouter()
  const { printData } = useSelector((state) => state.print)
  const { cuadre } = useSelector((state) => state.cuadre)
  const { userInfo } = useSelector((state) => state.user)

  window.onafterprint = function () {
    router.back()
  }

  useEffect(() => {
    handlePrint()
  }, [])

  const handlePrint = () => {
    setTimeout(() => {
      window.print()
    }, 1000)
  }

  console.log(printData)
  return (
    <div className='ticket'>
      <img className='m-auto' src={'/logo_letter.png'} alt='Logotipo' />

      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Caja Chica:</p>
        {cuadre?.entryMoney && <p>${cuadre.entryMoney}</p>}
      </div>
      {userInfo.position !== 'Hugo & Delivery' ? (
        <>
          <div style={{ marginTop: '10px', marginBottom: '10px' }}>
            <p>Efectivo sistema:</p>
            <p>
              $
              {printData.items
                .filter((i) => i.paymentMethod.includes('Efectivo'))
                .reduce((acc, c) => acc + c.totalPrice, 0)}
            </p>
          </div>
          <div style={{ marginTop: '10px', marginBottom: '10px' }}>
            <p>Tarjeta sistema:</p>
            <p>
              $
              {printData.items
                .filter((i) => i.paymentMethod.includes('Tarjeta'))
                .reduce((acc, c) => acc + c.totalPrice, 0)}
            </p>
          </div>
          <div style={{ marginTop: '10px', marginBottom: '10px' }}>
            <p>Transferencia:</p>
            <p>
              $
              {printData.items
                .filter((i) => i.paymentMethod.includes('Transferencia'))
                .reduce((acc, c) => acc + c.totalPrice, 0)}
            </p>
          </div>
        </>
      ) : (
        <>
          <div style={{ marginTop: '10px', marginBottom: '10px' }}>
            <p>Efectivo sistema:</p>
            <p>
              $
              {printData.items
                .filter((i) => i.paymentMethod.includes('Delivery'))
                .reduce((acc, c) => acc + c.totalPrice, 0)}
            </p>
          </div>
        </>
      )}
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Efectivo Cuadre:</p>
        <p>${printData.cash}</p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Tarjeta Cuadre:</p>
        <p>${printData.card}</p>
      </div>
      <div style={{ marginTop: '10px', marginBottom: '10px' }}>
        <p>Notas:</p>
        <p>{printData.notes}</p>
      </div>

      {printData &&
        printData?.items.map((i) => (
          <>
            <table>
              <thead>
                <tr>
                  <th></th>
                  <th>#</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{formatDate(new Date(i.createdAt), 'year')}</td>
                  <td>{i.number}</td>
                  <td>{i.totalPrice}</td>
                </tr>
              </tbody>
            </table>
          </>
        ))}
    </div>
  )
}

export default dynamic(() => Promise.resolve(PrintCuadre), { ssr: false })
