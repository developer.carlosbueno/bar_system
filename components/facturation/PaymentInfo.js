import { useSelector } from 'react-redux'
import dynamic from 'next/dynamic'

//Components
import { BsTrash } from 'react-icons/bs'
import { useState } from 'react'

function PaymentInfo({
  paymentMethod,
  setPaymentMethod,
  products,
  handleDelete,
  subtotal,
  itbis,
  tip,
  total,
  handleCreateOrder,
  office,
  setOffice,
  cash,
  card,
  transfer,
  setCash,
  setCard,
  setTransfer,
  handleAddPendingOrder,
  client,
  clientName,
  setClientName,
}) {
  const { userInfo } = useSelector((state) => state.user)

  const [openOffice, setOpenOffice] = useState(false)

  const handleOffice = () => {
    setOpenOffice(!openOffice)
    !openOffice ? setPaymentMethod('Oficina') : setPaymentMethod('')
  }

  return (
    <div className='bg-white payment_info_card'>
      <div className='head_info bg-green-300 h-20'>
        <p className='text-2xl font-bold'>Total</p>
        <p className='font-mediun text-xl'>${total.toFixed(2)}</p>
      </div>
      <div className='head_info h-10 border-b-2 border-gray-200'>
        <p className='font-medium'>Subtotal</p>
        <p>${subtotal.toFixed(2)}</p>
      </div>
      <div className='head_info h-10 border-b-2 border-gray-200'>
        <p>18% ITBIS</p>
        <p>${itbis.toFixed(2)}</p>
      </div>
      <div className='head_info h-10 border-b-2 border-gray-200'>
        <p>10% Propina</p>
        <p>${tip.toFixed(2)}</p>
      </div>

      <div className='p-3'>
        <div className='flex justify-between items-center mb-3'>
          <h4 className='text-lg'>Agregar Pago</h4>
          {userInfo?.position !== 'Hugo & Delivery' && (
            <button
              onClick={handleOffice}
              className=' underline underline-offset-1 text-blue-500'
            >
              Oficina
            </button>
          )}
        </div>
        {!openOffice ? (
          <>
            <div className='flex justify-between flex-wrap'>
              <div
                className={`flex-1 grid place-items-center py-4 bg-gray-200 rounded-3xl cursor-pointer ${
                  paymentMethod.includes('Efectivo')
                    ? 'border-2 border-blue-400'
                    : ''
                }`}
                onClick={() => setPaymentMethod('Efectivo')}
              >
                Efectivo
              </div>
              <div
                className={`flex-1 grid place-items-center py-4 mx-3 bg-gray-200 rounded-3xl cursor-pointer ${
                  paymentMethod.includes('Tarjeta')
                    ? 'border-2 border-blue-400'
                    : ''
                }`}
                onClick={() => setPaymentMethod('Tarjeta')}
              >
                Tarjeta
              </div>
              <div
                className={`flex-1 grid place-items-center py-4 bg-gray-200 rounded-3xl cursor-pointer ${
                  paymentMethod.includes('Transferencia')
                    ? 'border-2 border-blue-400'
                    : ''
                }`}
                onClick={() => setPaymentMethod('Transferencia')}
              >
                Transferencia
              </div>
            </div>
            {userInfo?.position === 'Hugo & Delivery' && (
              <div
                className={`flex-1 grid place-items-center py-4 mt-2 bg-gray-200 rounded-3xl cursor-pointer ${
                  paymentMethod.includes('Hugo')
                    ? 'border-2 border-blue-400'
                    : ''
                }`}
                onClick={() => setPaymentMethod('Hugo')}
              >
                Hugo
              </div>
            )}
          </>
        ) : (
          <select
            value={office}
            onChange={(e) => setOffice(e.target.value)}
            className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
          >
            <option value='Jorge'>Jorge</option>
            <option value='Victor'>Victor</option>
            <option value='Alfredo'>Alfredo</option>
            <option value='Tete'>Tete</option>\
          </select>
        )}
      </div>

      {paymentMethod.includes('Efectivo') && paymentMethod.length === 1 && (
        <div className='px-3 grid grid-cols-2 mb-3'>
          <div>
            <h3 className='mb-1 text-center'>Recibido</h3>
            <input
              type='number'
              placeholder='efectivo'
              value={cash}
              className='px-3 py-3 w-11/12 border-2 border-slate-500 outline-none rounded-lg text-center'
              onChange={(e) => setCash(e.target.value)}
            />
          </div>
          <div className='text-center'>
            <h3 className='mb-1'>Devuelta</h3>
            <div className='py-3 bg-red-300 rounded-3xl font-bold'>
              ${Math.round(cash - total)}
            </div>
          </div>
        </div>
      )}

      {paymentMethod.includes('Efectivo') &&
        paymentMethod.includes('Tarjeta') &&
        paymentMethod.length === 2 && (
          <div className='px-3 grid grid-cols-2 mb-3'>
            <div>
              <h3 className='mb-1 text-center'>Efectivo</h3>
              <input
                type='number'
                placeholder='efectivo'
                value={cash}
                className='px-3 py-3 w-11/12 border-2 border-slate-500 outline-none rounded-lg text-center'
                onChange={(e) => setCash(e.target.value)}
              />
            </div>
            <div>
              <h3 className='mb-1 text-center'>Tarjeta</h3>
              <input
                type='number'
                placeholder='efectivo'
                value={card}
                className='px-3 py-3 w-11/12 border-2 border-slate-500 outline-none rounded-lg text-center'
                onChange={(e) => setCard(e.target.value)}
              />
            </div>
          </div>
        )}

      {paymentMethod.includes('Efectivo') &&
        paymentMethod.includes('Transferencia') &&
        paymentMethod.length === 2 && (
          <div className='px-3 grid grid-cols-2 mb-3'>
            <div>
              <h3 className='mb-1 text-center'>Efectivo</h3>
              <input
                type='number'
                placeholder='efectivo'
                value={cash}
                className='px-3 py-3 w-11/12 border-2 border-slate-500 outline-none rounded-lg text-center'
                onChange={(e) => setCash(e.target.value)}
              />
            </div>
            <div>
              <h3 className='mb-1 text-center'>Transferencia</h3>
              <input
                type='number'
                placeholder='efectivo'
                value={transfer}
                className='px-3 py-3 w-11/12 border-2 border-slate-500 outline-none rounded-lg text-center'
                onChange={(e) => setTransfer(e.target.value)}
              />
            </div>
          </div>
        )}

      <div className='px-5'>
        <div className='flex items-center justify-between'>
          <h4 className='text-lg mb-3'>Lista</h4> <p>{client && client}</p>
        </div>
        <div className='py-2 min-height-200'>
          {products?.map((product) => (
            <div key={product._id} className='grid grid-cols-3 mb-2'>
              <p>{product.name}</p>
              <p className=' justify-self-center'>{product.qty}</p>
              <p
                className=' justify-self-end'
                onClick={() => handleDelete(product.name)}
              >
                <BsTrash />
              </p>
            </div>
          ))}
        </div>
      </div>
      <div className='px-4 mb-2'>
        <input
          type={'text'}
          placeholder='Nombre'
          className='input w-full rounded-md'
          onChange={(e) => setClientName(e.target.value)}
          value={clientName}
        />
      </div>
      <div className='grid grid-cols-2 gap-x-2 mx-3 pb-3'>
        <button
          onClick={handleAddPendingOrder}
          className='w-full py-5 font-medium rounded-xl bg-amber-400 hover:bg-amber-500'
        >
          Guardar
        </button>
        <button
          onClick={handleCreateOrder}
          className='w-full py-5 font-medium rounded-xl bg-green-300 hover:bg-green-400'
        >
          Facturar
        </button>
      </div>
    </div>
  )
}

export default dynamic(() => Promise.resolve(PaymentInfo), { ssr: false })
