import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

//Actions
import { deleteProduct } from 'store/actions/products'

const Table = ({ search, fillUpdateUser }) => {
  const { users } = useSelector((state) => state.userList)
  const dispatch = useDispatch()

  return (
    <>
      <div className='relative overflow-x-auto shadow-md sm:rounded-lg'>
        <table className='w-full text-sm text-left text-gray-500 dark:text-gray-400'>
          <thead className='text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400'>
            <tr>
              <th scope='col' className='px-6 py-3'>
                Name
              </th>
              <th scope='col' className='px-6 py-3'>
                Username
              </th>
              <th scope='col' className='px-6 py-3'>
                Posicion
              </th>
              <th scope='col' className='px-6 py-3'>
                Celular
              </th>
              <th scope='col' className='px-6 py-3'>
                <span className='sr-only'>Edit</span>
              </th>
            </tr>
          </thead>
          <tbody>
            {users?.map((user) => (
              <tr
                key={user._id}
                className='bg-white border-b dark:bg-gray-800 dark:border-gray-700'
              >
                <th
                  scope='row'
                  className='px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap'
                >
                  {user.name}
                </th>
                <td className='px-6 py-4'>{user.username}</td>
                <td className='px-6 py-4'>{user.position}</td>
                <td className='px-6 py-4'>{user.number}</td>
                <td className='px-6 py-4 text-right'>
                  <button
                    onClick={() => fillUpdateUser(user)}
                    className='font-medium text-blue-600 dark:text-blue-500 hover:underline'
                  >
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* <UpdateModal open={open} handleClose={handleClose} id={productId} /> */}
    </>
  )
}

export default Table
