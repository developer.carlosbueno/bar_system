import jwt from 'jsonwebtoken'

export function signToken(user) {
  return jwt.sign({ id: user._id }, process.env.JWT_SECRET)
}
