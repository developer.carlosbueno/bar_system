import nc from 'next-connect'
import Cuadre from '@models/cuadre'
import { success, fail } from '@utils/response'
import db from '@utils/db'
import { protect } from '../../../middlewares/authMiddlewares'

const handler = nc()

handler.use(protect)

handler.post(async (req, res) => {
  const { entryMoney } = req.body
  try {
    await db.connect()
    const cuadre = await Cuadre.create({
      user: req.user._id,
      entryMoney,
    })
    await db.disconnect()
    success(res, 201, cuadre)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

handler.get(async (req, res) => {
  try {
    await db.connect()
    const cuadres = await Cuadre.find()
      .limit(5)
      .populate({
        path: 'user',
        select: '-password -number',
      })
      .populate({
        path: 'orders',
      })
      .sort({ createdAt: -1 })
    await db.disconnect()
    success(res, 200, cuadres)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
