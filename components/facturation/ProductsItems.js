import addClassColor from '@utils/classColor'

function ProductsItems({ products, handleAddProduct, selectedCategory }) {
  let productsByCategory = products
    ?.filter((p) => p.category === selectedCategory.toLowerCase())
    .sort((a, b) => a.price - b.price)

  return (
    <div className='products__container w-full mb-20'>
      {productsByCategory?.map((item, idx) => {
        let color = addClassColor(item.price)
        return (
          <div
            key={item._id}
            className={`h-32 ${color} cursor-pointer grid place-items-center rounded-md`}
            onClick={() => handleAddProduct(item.name, item.price, 1, item._id)}
          >
            {item.name}
          </div>
        )
      })}
    </div>
  )
}

export default ProductsItems
