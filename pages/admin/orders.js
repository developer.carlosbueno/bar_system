import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'

//Components
import Layout from '@components/admin/Layout'
import Loading from '@components/Loading'
import BarChart from '@components/charts/BarChart'

//Actions
// import {
//   listOrders,
//   listTodayOrders,
//   listDatesOrders,
// } from 'store/actions/orders'
import { listDateCuadres, listTodayCuadres } from '../../store/actions/cuadre'
import { listUsers } from '../../store/actions/users'
import { getPrintData } from '../../store/actions/print'
import { listProducts } from 'store/actions/products'

//Components
import { FaCashRegister } from 'react-icons/fa'
import { RiNewspaperFill } from 'react-icons/ri'
import { BsCash, BsFillCreditCard2BackFill } from 'react-icons/bs'
import ExpandTable from '@components/ExpandTableAdmin'

const Orders = () => {
  const dispatch = useDispatch()
  const router = useRouter()

  const { cuadres, loading } = useSelector((state) => state.cuadres)
  const { cuadres: cuadresDate } = useSelector((state) => state.cuadresDate)
  const { users } = useSelector((state) => state.userList)
  // const { orders } = useSelector((state) => state.todayOrders)
  // const { orders: datesOrders } = useSelector((state) => state.datesOrders)
  const { products } = useSelector((state) => state.products)
  const [dateStart, setDateStart] = useState(
    new Date().toISOString().split('T')[0]
  )

  const [dateEnd, setDateEnd] = useState(new Date().toISOString().split('T')[0])
  const [chartData, setChartData] = useState([])
  const [orderPosition, setOrderPosition] = useState('All')
  const [actualOrders, setActualOrders] = useState([])
  const [actualCuadre, setActualCuadre] = useState([])
  const [loadingChartData, setLoadingChartData] = useState(false)

  let userWork = users?.filter(
    (i) => i.position === 'Cajera' || i.position === 'Hugo & Delivery'
  )

  let items = []
  let totalItems = []

  actualCuadre?.forEach((i) => {
    userWork?.filter((u) => u._id === i.user._id && items.push(i))
    totalItems = []
    items.forEach((i) => {
      totalItems = [...totalItems, ...i.orders]
    })
  })

  useEffect(() => {
    switch (orderPosition) {
      case 'All':
        setActualOrders(totalItems)
        return

      case 'Local':
        setActualOrders([
          ...totalItems.filter((o) => o.user.position === 'Cajera'),
        ])
        console.log('totalItems', totalItems)
        return

      case 'Hugo':
        setActualOrders([
          ...totalItems.filter(
            (o) =>
              o.user.position === 'Hugo & Delivery' &&
              o.paymentMethod.includes('Hugo')
          ),
        ])
        return

      case 'Delivery':
        setActualOrders(
          totalItems?.filter(
            (o) =>
              o.user.position === 'Hugo & Delivery' &&
              o.paymentMethod !== 'Hugo'
          )
        )
        return

      default:
        break
    }
  }, [orderPosition])

  console.log('totalItems', totalItems)
  // console.log('actualOrders', actualOrders)
  // console.log(cuadres)

  let cash = totalItems
    .filter((i) => i.paymentMethod.includes('Efectivo'))
    .reduce((acc, i) => acc + i.totalPrice, 0)

  let card = totalItems
    .filter((i) => i.paymentMethod.includes('Tarjeta'))
    .reduce((acc, i) => acc + i.totalPrice, 0)

  let total = cash + card

  useEffect(() => {
    dispatch(listTodayCuadres())
    dispatch(listUsers())
    // dispatch(listTodayOrders())
    dispatch(listProducts())
    getChartData()
  }, [])

  useEffect(() => {
    getCurrentCuadres()
    getChartData()
  }, [orderPosition, cuadres])

  const handleChangeDate = (e) => {
    const value = e.target.value
    if (e.target.name === 'dateStart') {
      setDateStart(value)
      dispatch(listDateCuadres(value, dateEnd))
      // dispatch(listDatesOrders(value, dateEnd))
    } else {
      setDateEnd(e.target.value)
      dispatch(listDateCuadres(dateStart, value))
      // dispatch(listDatesOrders(dateStart, value))
    }
  }

  const handlePrintCuadre = (items, cash, card, notes) => {
    dispatch(getPrintData(items, true, cash, card, notes))
  }

  const getChartData = () => {
    setLoadingChartData(true)
    const data = []
    let chartItem = {}
    // datesOrders?.length > 0 ? datesOrders : actualOrders
    products?.forEach((product) => {
      chartItem.name = product.name
      chartItem.qty = 0
      actualOrders?.forEach((order) => {
        order.orderItems.forEach((item) => {
          if (item.name === chartItem.name) {
            chartItem.qty = chartItem.qty + Number(item.qty)
          }
        })
      })
      data.push(chartItem)
      chartItem = {}
    })
    setChartData([...data])
    setLoadingChartData(false)
  }

  const getCurrentCuadres = () => {
    let currentCuadres = !cuadresDate ? cuadres : cuadresDate
    switch (orderPosition) {
      case 'All':
        setActualCuadre(currentCuadres)
        return

      case 'Local':
        setActualCuadre(
          currentCuadres?.filter((i) => i.user.position === 'Cajera')
        )
        return

      case 'Hugo':
        setActualCuadre(
          currentCuadres?.filter((o) => o.user.position === 'Hugo & Delivery')
        )
        return

      case 'Delivery':
        setActualCuadre(
          currentCuadres?.filter((i) => i.user.position === 'Hugo & Delivery')
        )

      default:
        break
    }
  }

  return (
    <Layout title='Orders' section='Orders'>
      <div className='flex'>
        <div className='w-60 text-sm font-medium mt-5 text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white'>
          <button
            onClick={() => setOrderPosition('All')}
            className={`block w-full px-4 py-2  border-b border-gray-200 rounded-t-lg cursor-pointer hover:bg-gray-100 hover:text-blue-700 dark:bg-gray-800 dark:border-gray-600 ${
              orderPosition === 'All' && 'bg-blue-700 text-white'
            }`}
          >
            All
          </button>
          <button
            onClick={() => setOrderPosition('Local')}
            className={`block w-full px-4 py-2 border-b border-gray-200 cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white ${
              orderPosition === 'Local' && 'bg-blue-700 text-white'
            }`}
          >
            Local
          </button>
          <button
            onClick={() => setOrderPosition('Delivery')}
            className={`block w-full px-4 py-2 border-b border-gray-200 cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white ${
              orderPosition === 'Delivery' && 'bg-blue-700 text-white'
            }`}
          >
            Delivery
          </button>
          <button
            onClick={() => setOrderPosition('Hugo')}
            className={`block w-full px-4 py-2 rounded-b-lg cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white ${
              orderPosition === 'Hugo' && 'bg-blue-700 text-white'
            }`}
          >
            Hugo
          </button>
        </div>

        {loading ? (
          <Loading />
        ) : (
          <div className='w-full'>
            <div className='my-3 px-8 flex items-center justify-between'>
              <div>
                <input
                  type='date'
                  value={dateStart}
                  name='dateStart'
                  onChange={handleChangeDate}
                  className='p-1 bg-gray-200 rounded-lg cursor-pointer outline-none mr-3 mb-3'
                />
                <input
                  type='date'
                  value={dateEnd}
                  name='dateEnd'
                  onChange={handleChangeDate}
                  className='p-1 bg-gray-200 rounded-lg cursor-pointer outline-none'
                />
              </div>

              <div className='flex justify-between'>
                <button
                  data-tooltip-target='tooltip-facturas'
                  className='flex bg-gray-300 px-4 py-2 items-center rounded-lg cursor-pointer'
                >
                  <div className='mr-5'>
                    <h2>{totalItems.length || 0}</h2>
                  </div>
                  <RiNewspaperFill className='text-2xl' />
                  <div
                    id='tooltip-facturas'
                    role='tooltip'
                    className='inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700'
                  >
                    Facturas
                    <div className='tooltip-arrow' data-popper-arrow></div>
                  </div>
                </button>
                <button
                  data-tooltip-target='tooltip-cash'
                  className='flex bg-gray-300 px-4 py-2 items-center rounded-lg cursor-pointer mx-5'
                >
                  <div className='mr-5'>
                    <h2>${Number(cash).toFixed(2) || 0}</h2>
                  </div>
                  <BsCash className='text-2xl' />
                  <div
                    id='tooltip-cash'
                    role='tooltip'
                    className='inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700'
                  >
                    Efectivo
                    <div className='tooltip-arrow' data-popper-arrow></div>
                  </div>
                </button>
                <button
                  data-tooltip-target='tooltip-card'
                  className='flex bg-gray-300 px-4 py-2 items-center rounded-lg cursor-pointer mr-5'
                >
                  <div className='mr-5'>
                    <h2>${Number(card).toFixed(2) || 0}</h2>
                  </div>
                  <BsFillCreditCard2BackFill className='text-2xl' />
                  <div
                    id='tooltip-card'
                    role='tooltip'
                    className='inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700'
                  >
                    Tarjeta
                    <div className='tooltip-arrow' data-popper-arrow></div>
                  </div>
                </button>
                <button
                  data-tooltip-target='tooltip-total'
                  className='flex bg-gray-300 px-4 py-2 items-center rounded-lg cursor-pointer'
                >
                  <div className='mr-5'>
                    <h2>${Number(total).toFixed(2) || 0}</h2>
                  </div>
                  <FaCashRegister className='text-2xl' />
                  <div
                    id='tooltip-total'
                    role='tooltip'
                    className='inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700'
                  >
                    Total
                    <div className='tooltip-arrow' data-popper-arrow></div>
                  </div>
                </button>
              </div>
            </div>
            <div className='px-8 mt-5'>
              {items && (
                <ExpandTable
                  info={items}
                  users={userWork}
                  handlePrintCuadre={handlePrintCuadre}
                />
              )}
              {!loadingChartData ? (
                <>
                  <div className='mt-6 flex justify-center'>
                    <button
                      onClick={getChartData}
                      className='bg-slate-200 hover:bg-slate-300 px-8 py-1 rounded-lg mr-2'
                    >
                      Refresh
                    </button>
                    <button
                      onClick={() =>
                        router.push({
                          pathname: '/admin/excelData',
                          query: { orders: JSON.stringify(chartData) },
                        })
                      }
                      className='bg-green-300 hover:bg-green-400 px-8 py-1 rounded-lg'
                    >
                      Excel
                    </button>
                  </div>
                  <BarChart chartData={chartData} />
                </>
              ) : (
                'Loading'
              )}
            </div>
          </div>
        )}
      </div>
    </Layout>
  )
}

export default Orders
