import nc from 'next-connect'
import Cuadre from '@models/cuadre'
import { success, fail } from '@utils/response'
import db from '@utils/db'
import { protect } from '../../../middlewares/authMiddlewares'

const handler = nc()

// handler.use(protect)

handler.post(async (req, res) => {
  const { from, to } = req.body

  try {
    await db.connect()
    const cuadres = await Cuadre.find({
      createdAt: {
        $gte: new Date(from),
        $lte: new Date(to),
      },
    })
      .populate({
        path: 'user',
        select: '-password -number',
      })
      .populate({
        path: 'orders',
      })
    await db.disconnect()
    success(res, 200, cuadres)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
