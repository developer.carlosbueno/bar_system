const addClassColor = (price) => {
  if (price <= 117.19) {
    return 'green'
  }
  if (price === 136.72) {
    return 'yellow'
  }
  if (price >= 156.25) {
    return 'red'
  }
}

export default addClassColor
