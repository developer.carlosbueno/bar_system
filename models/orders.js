import mongoose, { Schema, model } from 'mongoose'

const orderSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users',
      required: true,
    },
    number: {
      type: Number,
      default: 0,
    },
    orderItems: [
      {
        name: { type: String, required: true },
        price: { type: String, required: true },
        qty: { type: Number, required: true },
        product: {
          type: Schema.Types.ObjectId,
          required: true,
          ref: 'products',
        },
      },
    ],
    totalPrice: {
      type: Number,
      required: true,
    },
    paymentMethod: {
      type: Array,
      required: true,
    },
    amount: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
)

const Orders = mongoose.models.orders || model('orders', orderSchema)

export default Orders
