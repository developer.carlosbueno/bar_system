const categories1 = ['Empanadas', 'Bebidas', 'Dulces']
const categories2 = ['Todos', 'Efectivo', 'Tarjeta', 'Oficina']

function HeaderCategory({ section, selectedCategory, setSelectedCategory }) {
  let categories = section === 'cuadre' ? categories2 : categories1
  return (
    <div className='flex justify-around border-b-2 border-gray-200 h-10 mb-5'>
      {categories.map((category, idx) => (
        <div
          key={idx}
          className={`hover:border-b-4 border-b-red-400 w-full ${
            selectedCategory === category ? 'border-b-4' : ''
          }`}
          onClick={() => setSelectedCategory(category)}
        >
          {category}
        </div>
      ))}
    </div>
  )
}

export default HeaderCategory
