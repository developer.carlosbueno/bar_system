import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

//Actions
import { getMyCuadre, createCuadre } from '../store/actions/cuadre'
import { getPrintData } from 'store/actions/print'

//Components
import HeaderCategory from '@components/facturation/HeaderCategory'
import ExpandTable from '@components/ExpandTable'
import { useRouter } from 'next/router'

function Cuadre() {
  const dispatch = useDispatch()
  const router = useRouter()
  const { userInfo } = useSelector((state) => state.user)
  const { cuadre, loading } = useSelector((state) => state.cuadre)

  const [entryMoney, setEntryMoney] = useState('')
  const [selectedCategory, setSelectedCategory] = useState('')
  const [openCuadrar, setOpenCuadrar] = useState(false)
  const [cuadreCash, setCuadreCash] = useState('')
  const [cuadreCard, setCuadreCard] = useState('')
  const [cuadreNotes, setCuadreNotes] = useState('')

  useEffect(() => {
    if (!userInfo) {
      router.push('/login')
    }
    dispatch(getMyCuadre())
  }, [])

  const handleCreateCuadre = (e) => {
    e.preventDefault()
    dispatch(createCuadre(entryMoney))
  }

  function setFilter() {
    let s
    if (cuadre) {
      if (selectedCategory) {
        s = cuadre?.orders?.filter((i) => i.paymentMethod === selectedCategory)
        return s
      } else {
        return cuadre?.orders
      }
    }
  }

  const isToday = (someDate) => {
    const today = new Date()
    return (
      someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
    )
  }

  const handlePrintCuadre = (cash, card, notes) => {
    dispatch(getPrintData(cuadre?.orders, true, cash, card, notes))
    router.push('/print_cuadre')
  }

  let today = isToday(new Date(cuadre?.createdAt))
  let items = setFilter()

  let cash = items
    ?.filter((i) => i.paymentMethod.includes('Efectivo'))
    .reduce((acc, i) => acc + (i.amount || i.totalPrice), 0)
  let card = items
    ?.filter((i) => i.paymentMethod.includes('Tarjeta'))
    .reduce((acc, i) => acc + (i.amount || i.totalPrice), 0)
  let transfer = items
    ?.filter((i) => i.paymentMethod.includes('Transferencia'))
    .reduce((acc, i) => acc + (i.amount || i.totalPrice), 0)

  return (
    <div className='flex py-10 px-10'>
      <div className='w-3/4  m-auto mb-20'>
        <div className='flex items-center justify-between mb-10 h-14'>
          <h1 className='text-3xl font-medium mr-10'>Cuadre</h1>

          <button
            onClick={() => setOpenCuadrar(true)}
            className='bg-blue-400 px-12 py-3 rounded-lg text-white hover:bg-blue-500'
          >
            Cuadrar
          </button>
        </div>
        {!loading && !openCuadrar ? (
          <>
            <HeaderCategory
              section={'cuadre'}
              selectedCategory={selectedCategory}
              setSelectedCategory={setSelectedCategory}
            />
            {today ? (
              <ExpandTable info={items} />
            ) : (
              <div className=' w-fit m-auto bg-gray-200 p-10 rounded-lg mt-24 shadow-lg'>
                <form className='flex flex-col' onSubmit={handleCreateCuadre}>
                  <label className='text-xl font-medium mb-3'>
                    Dinero inicial
                  </label>
                  <div>
                    <input
                      type={'number'}
                      className='py-3 px-3 outline-none mr-3 rounded-md border-2 border-gray-600'
                      onChange={(e) => setEntryMoney(e.target.value)}
                      value={entryMoney}
                    />
                    <button className='px-8 py-3 bg-green-400 rounded-md hover:bg-green-500 hover:font-medium'>
                      Iniciar
                    </button>
                  </div>
                </form>
              </div>
            )}
          </>
        ) : (
          'Loading'
        )}
        {openCuadrar && (
          <div className=''>
            <div className='flex flex-col w-1/2 m-auto'>
              <input
                placeholder='Efectivo'
                className='p-2 outline-none border-2 border-gray-300 rounded-xl mb-3'
                value={cuadreCash}
                onChange={(e) => setCuadreCash(e.target.value)}
              />
              <input
                placeholder='Tarjeta'
                className='p-2 outline-none border-2 border-gray-300 rounded-xl mb-3'
                value={cuadreCard}
                onChange={(e) => setCuadreCard(e.target.value)}
              />
              <textarea
                placeholder='Notas'
                className='p-2 outline-none border-2 border-gray-300 rounded-xl mb-3'
                value={cuadreNotes}
                onChange={(e) => setCuadreNotes(e.target.value)}
              />
            </div>
            <div className='w-1/2 m-auto flex'>
              <button
                onClick={() => setOpenCuadrar(false)}
                className='bg-red-400 px-12 py-3 rounded-lg text-white w-full mr-5 hover:bg-red-500'
              >
                Regresar
              </button>
              <button
                onClick={() =>
                  handlePrintCuadre(cuadreCash, cuadreCard, cuadreNotes)
                }
                className='bg-blue-400 px-12 py-3  rounded-lg text-white w-full hover:bg-blue-500'
              >
                Terminar
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default Cuadre
