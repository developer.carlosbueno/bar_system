import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loginUser } from '../store/actions/users'
import { useSnackbar } from 'notistack'

function Login() {
  const dispatch = useDispatch()
  const router = useRouter()
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()
  const { userInfo, error, loading } = useSelector((state) => state.user)

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    closeSnackbar()
    if (userInfo) {
      enqueueSnackbar('has iniciado sesion', { variant: 'success' })
      router.push('/facturation')
    }
    if (error) {
      enqueueSnackbar(error, { variant: 'error' })
    }
  }, [userInfo, error])

  const handleLoginUser = (e) => {
    e.preventDefault()
    dispatch(loginUser(username, password))
  }

  return (
    <div className='grid place-items-center h-full'>
      <form style={{ width: '400px' }} onSubmit={handleLoginUser}>
        <h1 className=' text-4xl mb-10 w-full text-center font-medium'>
          Login
        </h1>
        <div className='flex flex-col mb-5'>
          <label className='mb-1 text-sm text-gray-700'>Usuario</label>
          <input
            type='text'
            placeholder='usuario'
            className='p-2 outline-none border-2 border-gray-300 rounded-xl'
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className='flex flex-col mb-5'>
          <label className='mb-1 text-sm text-gray-700'>Contraseña</label>
          <input
            type='password'
            placeholder='contraseña'
            className='p-2 outline-none border-2 border-gray-300 rounded-xl'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <button
          type='submit'
          disabled={loading}
          className=' bg-blue-500 w-full h-10 font-medium text-white rounded-3xl hover:bg-blue-600'
        >
          Inicar sesion
        </button>
      </form>
    </div>
  )
}

export default Login
