import isToday from '@utils/isToday'
import axios from 'axios'

export const actions = {
  CUADRE_CREATE_REQUEST: 'CUADRE_CREATE_REQUEST',
  CUADRE_CREATE_SUCCESS: 'CUADRE_CREATE_SUCCESS',
  CUADRE_CREATE_FAIL: 'CUADRE_CREATE_FAIL',
  CUADRE_UPDATE_REQUEST: 'CUADRE_UPDATE_REQUEST',
  CUADRE_UPDATE_SUCCESS: 'CUADRE_UPDATE_SUCCESS',
  CUADRE_UPDATE_FAIL: 'CUADRE_UPDATE_FAIL',
  CUADRE_MY_REQUEST: 'CUADRE_MY_REQUEST',
  CUADRE_MY_SUCCESS: 'CUADRE_MY_SUCCESS',
  CUADRE_MY_FAIL: 'CUADRE_MY_FAIL',
  CUADRE_TODAY_REQUEST: 'CUADRE_TODAY_REQUEST',
  CUADRE_TODAY_SUCCESS: 'CUADRE_TODAY_SUCCESS',
  CUADRE_TODAY_FAIL: 'CUADRE_TODAY_FAIL',
  CUADRE_DATE_REQUEST: 'CUADRE_DATE_REQUEST',
  CUADRE_DATE_SUCCESS: 'CUADRE_DATE_SUCCESS',
  CUADRE_DATE_FAIL: 'CUADRE_DATE_FAIL',
}

export const createCuadre = (entryMoney) => async (dispatch, getState) => {
  dispatch({ type: actions.CUADRE_CREATE_REQUEST })
  try {
    const { userInfo } = getState().user

    let config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.post('/api/cuadres', { entryMoney }, config)
    dispatch({ type: actions.CUADRE_CREATE_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.CUADRE_CREATE_FAIL, payload: error.message })
  }
}

export const getMyCuadre = () => async (dispatch, getState) => {
  dispatch({ type: actions.CUADRE_MY_REQUEST })
  try {
    const { userInfo } = getState().user

    let config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.get(`/api/cuadres/${userInfo._id}`, config)
    dispatch({ type: actions.CUADRE_MY_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.CUADRE_MY_FAIL, payload: error.message })
  }
}

export const listTodayCuadres = () => async (dispatch, getState) => {
  dispatch({ type: actions.CUADRE_TODAY_REQUEST })
  try {
    const { userInfo } = getState().user

    let config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.get(`/api/cuadres`, config)
    const todayCuadres = data.body.filter((p) => isToday(new Date(p.createdAt)))
    dispatch({ type: actions.CUADRE_TODAY_SUCCESS, payload: todayCuadres })
  } catch (error) {
    dispatch({ type: actions.CUADRE_TODAY_FAIL, payload: error.message })
  }
}

export const listDateCuadres = (start, end) => async (dispatch, getState) => {
  dispatch({ type: actions.CUADRE_DATE_REQUEST })
  try {
    const { userInfo } = getState().user

    let config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.post(
      `/api/cuadres/dates`,
      { from: start, to: end },
      config
    )
    dispatch({ type: actions.CUADRE_DATE_SUCCESS, payload: data.body })
  } catch (error) {
    dispatch({ type: actions.CUADRE_DATE_FAIL, payload: error.message })
  }
}
