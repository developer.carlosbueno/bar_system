import nc from 'next-connect'
import Cuadre from '@models/cuadre'
import { success, fail } from '@utils/response'
import db from '@utils/db'

const handler = nc()

handler.get(async (req, res) => {
  try {
    await db.connect()
    let cuadre = await Cuadre.findOne({
      user: req.query.id,
    })
      .sort('-createdAt')
      .populate({
        path: 'user',
        select: '-password -number',
      })
      .populate('orders')
    await db.disconnect()
    success(res, 200, cuadre)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
