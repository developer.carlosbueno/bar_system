import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'

//Components
import Navbar from './Navbar'

function Layout({ children, title, description, section }) {
  const router = useRouter()

  const { userInfo } = useSelector((state) => state.user)

  useEffect(() => {
    if (!userInfo?.isAdmin) {
      router.push('/')
    }
  }, [])

  return (
    <div>
      <Head>
        <title>{title ? title : 'Panel '}</title>
      </Head>

      <Navbar section={section} />

      <div className='container mx-auto main__body'>{children}</div>
    </div>
  )
}

export default Layout
