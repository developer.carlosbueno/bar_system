//Components
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import BottomNav from './BottomNav'
import SideNav from './SideNav'

function Layout({ children }) {
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const { userInfo } = useSelector((state) => state.user)

  useEffect(() => {
    if (userInfo?.isAdmin) {
      router.pathname.includes('admin') ? '' : router.push('/admin')
    }
  }, [router.pathname])

  return (
    <div style={{ height: '100vh' }} className='relative'>
      {children}
      {userInfo && (
        <>
          <SideNav open={open} setOpen={setOpen} />
          <BottomNav open={open} setOpen={setOpen} />
        </>
      )}
    </div>
  )
}

export default Layout
