import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import cookies from 'js-cookie'

//Reducers
import {
  listProductReducer,
  detailProductReducer,
  createProductReducer,
  updateProductReducer,
} from './reducers/products'
import {
  userLoginReducer,
  userRegisterReducer,
  userListReducer,
  userDetailsReducer,
  userUpdateReducer,
} from './reducers/users'
import { printReducer } from './reducers/print'
import {
  orderListReducer,
  orderMyListReducer,
  orderReducer,
  orderMonthListReducer,
  orderDatesListReducer,
  orderTodayListReducer,
} from './reducers/orders'
import {
  cuadreReducer,
  cuadresReducer,
  cuadresDateReducer,
} from './reducers/cuadres'

const localUser = cookies.get('userInfo')
  ? JSON.parse(cookies.get('userInfo'))
  : null

const reducers = combineReducers({
  products: listProductReducer,
  product: detailProductReducer,
  createdProduct: createProductReducer,
  updatedProduct: updateProductReducer,
  user: userLoginReducer,
  userRegister: userRegisterReducer,
  userDetails: userDetailsReducer,
  userList: userListReducer,
  userUpdate: userUpdateReducer,
  print: printReducer,
  orders: orderListReducer,
  createdOrder: orderReducer,
  myOrders: orderMyListReducer,
  todayOrders: orderTodayListReducer,
  monthOrders: orderMonthListReducer,
  datesOrders: orderDatesListReducer,
  cuadre: cuadreReducer,
  cuadres: cuadresReducer,
  cuadresDate: cuadresDateReducer,
})

let initialState = {
  user: {
    userInfo: localUser,
  },
  products: [],
}

const store = createStore(
  reducers,
  initialState,
  composeWithDevTools(applyMiddleware(thunk))
)

export default store
