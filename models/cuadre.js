import mongoose, { Schema, model } from 'mongoose'

const cuadreSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users',
      required: true,
    },
    orders: [
      {
        type: Schema.Types.ObjectId,
        ref: 'orders',
      },
    ],
    entryMoney: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
)

const Cuadre = mongoose.models.cuadres || model('cuadres', cuadreSchema)

export default Cuadre
