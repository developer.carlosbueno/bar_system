import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    number: {
      type: String,
      required: false,
    },
    position: {
      type: String,
      required: false,
    },
    isAdmin: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: true,
  }
)

userSchema.methods.matchPassword = async function (enteredPassword) {
  if (this.password.length > 15) {
    return await bcrypt.compare(enteredPassword, this.password)
  }
  return enteredPassword === this.password
}

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next()
  }

  if (this.isAdmin) this.password = await bcrypt.hash(this.password, 10)

  next()
})

let User = mongoose.models.users || mongoose.model('users', userSchema)

export default User
