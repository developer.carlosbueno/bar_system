export const actions = {
  GET_PRINT_DATA: 'GET_PRINT_DATA',
  FORMAT_PRINT_DATA: 'FORMAT_PRINT_DATA',
}

export const getPrintData =
  (info, cuadre, cash, card, notes) => (dispatch, getData) => {
    const {
      user: { userInfo },
    } = getData()

    if (!cuadre) {
      dispatch({
        type: actions.GET_PRINT_DATA,
        payload: { ...info, user: userInfo.username },
      })
    } else {
      dispatch({
        type: actions.GET_PRINT_DATA,
        payload: { items: [...info], cash, card, notes },
      })
    }
  }

export const removePrintData = () => (dispatch) => {
  dispatch({ type: actions.FORMAT_PRINT_DATA })
}
