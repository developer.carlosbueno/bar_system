import nc from 'next-connect'
import Product from '@models/products'
import { success, fail } from '@utils/response'
import db from '@utils/db'

const handler = nc()

handler.post(async (req, res) => {
  const { name, price, category } = req.body
  try {
    await db.connect()
    const product = await Product.create({
      name,
      price,
      category,
    })
    await db.disconnect()
    success(res, 201, product)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

handler.get(async (req, res) => {
  try {
    await db.connect()
    const products = await Product.find()
    await db.disconnect()
    success(res, 200, products)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
