import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useSnackbar } from 'notistack'
import { useRouter } from 'next/router'
import cookies from 'js-cookie'

//Components
import HeaderCategory from '@components/facturation/HeaderCategory'
import PaymentInfo from '@components/facturation/PaymentInfo'
import ProductsItems from '@components/facturation/ProductsItems'
import { listProducts } from 'store/actions/products'
import { createOrder } from 'store/actions/orders'
import { getPrintData } from 'store/actions/print'

function Facturation() {
  const dispatch = useDispatch()
  const router = useRouter()
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()

  const { products } = useSelector((state) => state.products)
  const { userInfo } = useSelector((state) => state.user)

  const [selectedCategory, setSelectedCategory] = useState('empanadas')
  const [selectedProducts, setSelectedProducts] = useState([])
  const [paymentMethod, setPaymentMethod] = useState([])
  const [office, setOffice] = useState('Jorge')
  const [cash, setCash] = useState(0)
  const [card, setCard] = useState(0)
  const [transfer, setTransfer] = useState(0)
  const [pendingOrders, setPendingOrders] = useState(
    cookies.get('pendingOrders') ? JSON.parse(cookies.get('pendingOrders')) : []
  )
  const [client, setClient] = useState('')
  const [clientName, setClientName] = useState('')

  let subtotal = selectedProducts.reduce((acc, current) => {
    return acc + current.price * current.qty
  }, 0)
  let itbis = subtotal * 0.18
  let tip = subtotal * 0.1
  let total = subtotal + itbis + tip
  let itemsQty = selectedProducts.reduce((acc, current) => acc + current.qty, 0)

  const items = selectedProducts

  useEffect(() => {
    if (!userInfo) {
      router.push('/login')
    }
    closeSnackbar()
    dispatch(listProducts())
  }, [userInfo])

  useEffect(() => {
    cookies.set('pendingOrders', JSON.stringify(pendingOrders))
  }, [pendingOrders])

  const handleSetPaymentMethod = (method) => {
    const current = [...paymentMethod]
    const exist = paymentMethod.filter((i) => i === method)[0]
    if (exist) {
      setPaymentMethod([...current.filter((i) => i !== method)])
    } else {
      setPaymentMethod([...paymentMethod, method])
    }
  }

  const handleAddProduct = (name, price, qty, id) => {
    let exist = selectedProducts.filter((product) => product.name === name)[0]
    let currentItems
    if (exist) {
      exist.qty = exist.qty + 1
      currentItems = [...selectedProducts]
    } else {
      currentItems = [...selectedProducts, { name, price, qty, product: id }]
    }
    setSelectedProducts(currentItems)
  }

  const handleDelete = (name) => {
    let exist = selectedProducts.filter((product) => product.name === name)[0]
    let currentItems
    if (exist) {
      if (exist.qty === 1) {
        console.log(exist)
        let items = selectedProducts.filter((item) => item.name !== exist.name)
        currentItems = [...items]
      } else {
        exist.qty = exist.qty - 1
        currentItems = [...selectedProducts]
      }
    } else {
      currentItems = [...selectedProducts]
    }
    setSelectedProducts(currentItems)
  }

  const handleAddPendingOrder = () => {
    closeSnackbar()
    let exist = pendingOrders.filter((i) => i.name === client)[0]
    let newArray
    if (exist) {
      exist.order = selectedProducts
      newArray = [...pendingOrders]
    } else {
      if (!clientName) {
        enqueueSnackbar('Escriba un nombre para la orden', { variant: 'error' })
      } else {
        newArray = [
          ...pendingOrders,
          { order: selectedProducts, name: clientName },
        ]
      }
    }
    setClientName('')
    setPendingOrders(newArray)
    enqueueSnackbar('Orden agregada', {
      variant: 'success',
    })
    setSelectedProducts([])
    setClient('')
  }

  const handleRemovePendingOrder = (order) => {
    let newArray
    if (order) {
      if (window.confirm(`Seguro de borrar la orden ${order}?`)) {
        newArray = pendingOrders.filter((i) => i.name !== order)
      } else return
    } else {
      newArray = pendingOrders.filter((i) => i.name !== client)
    }
    setPendingOrders(newArray)
    setClient('')
  }

  const handleCreateOrder = (e) => {
    e.preventDefault()
    const hugoItems = selectedProducts.map((i) => ({
      name: i.name,
      price: 0,
      qty: i.qty,
      product: i.product,
    }))
    if (window.confirm('Estas seguro de realizar esta orden?')) {
      if (selectedProducts.length > 0 && paymentMethod.length > 0) {
        dispatch(
          createOrder({
            orderItems: paymentMethod === 'Hugo' ? hugoItems : selectedProducts,
            totalPrice: paymentMethod === 'Hugo' ? 0 : total.toFixed(2),
            paymentMethod,
            cash,
            card,
            transfer,
          })
        )
        enqueueSnackbar('Orden realizada!', {
          variant: 'success',
        })
        if (paymentMethod.includes('Efectivo')) {
          dispatch(
            getPrintData({
              orderItems: selectedProducts,
              totalPrice: total.toFixed(2),
              itbis: itbis.toFixed(2),
              tip: tip.toFixed(2),
              time: new Date(),
              paymentMethod,
              cash,
            })
          )
        } else if (paymentMethod.includes('Oficina')) {
          dispatch(
            getPrintData({
              orderItems: selectedProducts,
              totalPrice: 0,
              itbis: 0,
              tip: 0,
              time: new Date(),
              paymentMethod,
              office,
            })
          )
        } else if (paymentMethod === 'Hugo') {
          dispatch(
            getPrintData({
              orderItems: hugoItems,
              totalPrice: 0,
              itbis: 0,
              tip: 0,
              time: new Date(),
            })
          )
          router.push('/print')
          return
        } else {
          dispatch(
            getPrintData({
              orderItems: selectedProducts,
              totalPrice: total.toFixed(2),
              itbis: itbis.toFixed(2),
              tip: tip.toFixed(2),
              time: new Date(),
            })
          )
        }
        setSelectedProducts([])
        handleRemovePendingOrder()
        router.push('/print')
      }
      if (selectedProducts.length <= 0)
        enqueueSnackbar('Agregue articulos para facturar', {
          variant: 'error',
        })
      if (paymentMethod === '')
        enqueueSnackbar('Eliga un metodo de pago', { variant: 'error' })
    }
  }

  const handleClientChange = (name) => {
    const current = pendingOrders.filter((i) => i.name === name)[0]
    if (client === name) {
      setClient('')
      setSelectedProducts([])
      return
    }
    setClient(name)
    setSelectedProducts(current.order)
  }

  return (
    <div className='flex py-10 px-10'>
      <div className='w-3/4  mr-5'>
        <div className='flex items-center mb-10 h-14'>
          <h1 className='text-3xl font-medium mr-10'>Facturacion</h1>
          {pendingOrders.map((order, idx) => (
            <div className='relative' key={idx}>
              <div
                className={`px-8 py-3 bg-amber-400 rounded-lg mr-3 cursor-pointer hover:bg-amber-500 ${
                  client === order.name
                    ? 'bg-amber-500 border-2 border-gray-500'
                    : ''
                }`}
                onClick={() => handleClientChange(order.name)}
              >
                {order.name}
              </div>
              <div
                onClick={() => handleRemovePendingOrder(order.name)}
                className='absolute -top-2 bg-red-100 w-6 h-6 text-xs rounded-full right-0 grid place-items-center cursor-pointer'
              >
                x
              </div>
            </div>
          ))}
        </div>
        <HeaderCategory
          selectedCategory={selectedCategory}
          setSelectedCategory={setSelectedCategory}
        />
        <ProductsItems
          handleAddProduct={handleAddProduct}
          selectedCategory={selectedCategory}
          products={products}
        />
      </div>
      <div className='w-1/4 '>
        <PaymentInfo
          products={items}
          handleDelete={handleDelete}
          subtotal={subtotal}
          itbis={itbis}
          tip={tip}
          total={total}
          itemsQty={itemsQty}
          paymentMethod={paymentMethod}
          setPaymentMethod={handleSetPaymentMethod}
          handleCreateOrder={handleCreateOrder}
          setOffice={setOffice}
          office={office}
          cash={cash}
          card={card}
          transfer={transfer}
          setCash={setCash}
          setCard={setCard}
          setTransfer={setTransfer}
          handleAddPendingOrder={handleAddPendingOrder}
          client={client}
          clientName={clientName}
          setClientName={setClientName}
        />
      </div>
    </div>
  )
}

export default Facturation
