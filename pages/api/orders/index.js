import nc from 'next-connect'
import Orders from '@models/orders'
import Cuadre from '@models/cuadre'
import { success, fail } from '@utils/response'
import db from '@utils/db'
import { protect } from '../../../middlewares/authMiddlewares'

const getAmount = (method, cash, card, transfer) => {
  if (method === 'Efectivo') {
    return cash
  } else if (method === 'Tarjeta') {
    return card
  } else {
    return transfer
  }
}

const handler = nc()

handler.use(protect)

handler.post(async (req, res) => {
  const { totalPrice, orderItems, paymentMethod, cash, card, transfer } =
    req.body
  let ids = []
  try {
    if (orderItems && orderItems.length === 0) {
      fail(res, 400, 'No order items')
    } else {
      await db.connect()
      const cuadres = await Cuadre.find({
        user: req.user._id,
      }).populate('orders')

      const cuadre = cuadres.slice(-1)[0]
      let createdOrder
      if (paymentMethod.lenght === 1) {
        createdOrder = await Orders.create({
          user: req.user._id,
          totalPrice,
          orderItems,
          paymentMethod: paymentMethod[0],
          amount: getAmount(paymentMethod[0], cash, card, transfer),
          number: cuadre.orders.length + 1,
        })
        cuadre.orders = [createdOrder._id, ...cuadre.orders]
        await cuadre.save()
        await db.disconnect()
        success(res, 201, 'Order created')
      } else {
        for (let index = 0; index < paymentMethod.length; index++) {
          if (index == 0) {
            createdOrder = await Orders.create({
              user: req.user._id,
              totalPrice,
              orderItems,
              paymentMethod: paymentMethod[index],
              amount: getAmount(paymentMethod[index], cash, card, transfer),
              number: cuadre.orders[0]?.number + 1 || 1,
            })
          } else {
            createdOrder = await Orders.create({
              user: req.user._id,
              totalPrice: 0,
              orderItems: orderItems.map((i) => {
                return { ...i, qty: 0 }
              }),
              paymentMethod: paymentMethod[index],
              amount: getAmount(paymentMethod[index], cash, card, transfer),
              number: cuadre.orders[0].number + 1,
            })
          }
          ids.push(createdOrder._doc._id)
        }
      }
      cuadre.orders = [...ids, ...cuadre.orders]
      await cuadre.save()
      await db.disconnect()
      success(res, 201, 'Order created')
    }
  } catch (error) {
    fail(res, 500, error.message)
  }
})

handler.get(async (req, res) => {
  try {
    await db.connect()
    const orders = await Orders.find().limit(7500)
    await db.disconnect()
    success(res, 200, orders)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
