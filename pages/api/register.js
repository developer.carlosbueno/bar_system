import nc from 'next-connect'
import User from '@models/users'
import db from '@utils/db'
import { success, fail } from '@utils/response'
import { signToken } from '@utils/jwt'

const handler = nc()

handler.post(async (req, res) => {
  const { username, password } = req.body

  try {
    await db.connect()
    const exist = await User.findOne({ username })
    await db.disconnect()
    if (exist) {
      fail(res, 400, 'User already exist!.')
      return
    }
    const user = await User.create({
      username: 'manager',
      password: 'bendito',
      isAdmin: true,
    })
    success(res, 200, {
      _id: user._id,
      username: user.username,
      isAdmin: user.isAdmin,
      token: signToken(user),
    })
  } catch (error) {
    fail(res, 400, error.message)
  }
})

export default handler
