import React, { useState, useEffect } from 'react'
import {
  Chart as ChartJS,
  BarElement,
  CategoryScale,
  LinearScale,
  Tooltip,
  ArcElement,
} from 'chart.js'

import { Bar } from 'react-chartjs-2'

ChartJS.register(BarElement, CategoryScale, LinearScale, Tooltip, ArcElement)

const BarChart = ({ chartData }) => {
  let orderChart = chartData.sort((a, b) => b.qty - a.qty)
  var data = {
    labels: orderChart.map((data) => data.name),
    datasets: [
      {
        label: 'Empanadas',
        data: orderChart.map((data) => data.qty),
        fill: true,
        backgroundColor: 'rgba(75,192,192,0.2)',
        borderColor: 'rgba(75,192,192,1)',
      },
    ],
  }

  var options = {
    maintainAspectRatio: false,
    scales: {},
    legend: {
      labels: {
        fontSize: 25,
      },
    },
  }

  return (
    <div>
      <Bar data={data} height={400} options={options} />
    </div>
  )
}

export default BarChart
