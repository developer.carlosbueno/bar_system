import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

//Components
import Layout from '@components/admin/Layout'
import EmployeesTable from '@components/EmployeesTable'

//Actions

import { listUsers, registerUser } from 'store/actions/users'

function Users() {
  const dispatch = useDispatch()

  const [name, setName] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [position, setPosition] = useState('')
  const [number, setNumber] = useState('')
  const [isAdmin, setIsAdmin] = useState(false)
  const [userId, setUserId] = useState('')

  useEffect(() => {
    dispatch(listUsers())
  }, [])

  const fillUpdateUser = (user) => {
    setUserId(user._id)
    setName(user.name)
    setUsername(user.username)
    setPassword(user.password)
    setPosition(user.position)
    setNumber(user.number)
    setIsAdmin(user.isAdmin)
  }

  const removeInputs = () => {
    setUserId('')
    setUsername('')
    setPassword('')
    setName('')
    setPosition('')
    setNumber('')
    setIsAdmin(false)
  }

  const handleCreate = (e) => {
    dispatch(
      registerUser({
        name,
        position,
        number,
        isAdmin,
      })
    )
  }

  return (
    <Layout title='users' section='Users'>
      <div className='flex px-8 mt-5'>
        <div className='w-3/4'>
          <div className='pb-4'>
            <label htmlFor='table-search' className='sr-only'>
              Search
            </label>
            <div className='relative mt-1'>
              <div className='flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none'>
                <svg
                  className='w-5 h-5 text-gray-500 dark:text-gray-400'
                  fill='currentColor'
                  viewBox='0 0 20 20'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path
                    fillRule='evenodd'
                    d='M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z'
                    clipRule='evenodd'
                  ></path>
                </svg>
              </div>
              <input
                type='text'
                id='table-search'
                className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-80 pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
                placeholder='Search for items'
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
          </div>
          <EmployeesTable fillUpdateUser={fillUpdateUser} />
        </div>
        <div className='w-1/4 shadow-lg p-3 ml-3'>
          <h3 className='text-center text-2xl mb-5'>Crear usuario</h3>

          <form>
            <div className='mb-6'>
              <label
                htmlFor='name'
                className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'
              >
                Name
              </label>
              <input
                id='name'
                className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light'
                placeholder='Pedro Ramires'
                value={name}
                onChange={(e) => setName(e.target.value)}
                required=''
              />
            </div>
            {userId && (
              <>
                <div className='mb-6'>
                  <label
                    htmlFor='username'
                    className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'
                  >
                    Username
                  </label>
                  <input
                    id='username'
                    className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light'
                    placeholder='Pedro Ramires'
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    required=''
                  />
                </div>
                <div className='mb-6'>
                  <label
                    htmlFor='password'
                    classPassword='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'
                  >
                    Password
                  </label>
                  <input
                    id='password'
                    className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light'
                    placeholder='Pedro Ramires'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required=''
                  />
                </div>
              </>
            )}
            <div className='mb-6'>
              <label
                htmlFor='position'
                className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'
              >
                Position
              </label>
              <select
                id='position'
                value={position}
                onChange={(e) => setPosition(e.target.value)}
                className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
              >
                <option value='Head Chef'>Head Chef</option>
                <option value='Chef'>Chef</option>
                <option value='Manager'>Manager</option>
                <option value='Hugo & Delivery'>Hugo & Delivery</option>
                <option value='Steward'>Steward</option>
                <option value='Cajera'>Cajera</option>
              </select>
            </div>
            <div className='mb-6'>
              <label
                htmlFor='repeat-password'
                className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'
              >
                Numero
              </label>
              <input
                type='text'
                id='repeat-password'
                className='shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light'
                value={number}
                onChange={(e) => setNumber(e.target.value)}
                required=''
              />
            </div>
            <div className='flex items-start mb-6'>
              <div className='flex items-center h-5'>
                <input
                  id='admin'
                  aria-describedby='admin'
                  type='checkbox'
                  className='w-4 h-4 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800'
                  value={isAdmin}
                  onChange={() => setIsAdmin(!isAdmin)}
                  required=''
                />
              </div>
              <div className='ml-3 text-sm'>
                <label
                  htmlFor='admin'
                  className='font-medium text-gray-900 dark:text-gray-300'
                >
                  Admin
                </label>
              </div>
            </div>
            <div className='grid grid-cols-2 gap-x-5'>
              <button
                type='button'
                onClick={removeInputs}
                className='py-4 rounded-xl bg-red-400 hover:bg-red-500'
              >
                Cancelar
              </button>
              <button
                type='submit'
                className='py-4 rounded-xl bg-green-400 hover:bg-green-500'
                onClick={handleCreate}
              >
                Guardar
              </button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  )
}

export default Users
