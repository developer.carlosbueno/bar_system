import nc from 'next-connect'
import User from '@models/users'
import { success, fail } from '@utils/response'
import db from '@utils/db'
import { protect } from '../../../middlewares/authMiddlewares'

const handler = nc()

handler.post(async (req, res) => {
  const { username, password, name, position, number, isAdmin } = req.body

  console.log(req.body)

  try {
    let user = await User.findOne({ username })
    if (user) return success(res, 400, 'ya existe este usuario.')

    let newUser = await User.create({
      name,
      position,
      number,
      isAdmin,
      username,
      password,
    })

    success(res, 201, newUser)
  } catch (error) {
    fail(res, 400, error.message)
  }
})

handler.get(async (req, res) => {
  try {
    await db.connect()
    const users = await User.find()
    await db.disconnect()
    success(res, 200, users)
  } catch (error) {
    fail(res, 500, error.message)
  }
})

export default handler
