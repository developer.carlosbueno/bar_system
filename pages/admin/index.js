import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'

//Actions
import { listProducts } from 'store/actions/products'
import {
  listOrders,
  listTodayOrders,
  listMonthOrders,
  listDatesOrders,
} from 'store/actions/orders'
import { listUsers } from 'store/actions/users'

//Components
import Layout from '@components/admin/Layout'
import Loading from '@components/Loading'

//Utils
import isToday from '@utils/isToday'

//Charts
import BarChart from '@components/charts/BarChart'
import { listTodayCuadres } from 'store/actions/cuadre'
// import LineChart from '@components/charts/LineChart'
// import DoughnutChart from '@components/charts/DoughnutChart'

function Home() {
  const dispatch = useDispatch()
  const router = useRouter()

  const { products } = useSelector((state) => state.products)
  // const { orders, loading } = useSelector((state) => state.monthOrders)
  const { orders: datesOrders } = useSelector((state) => state.datesOrders)
  const { orders, loading } = useSelector((state) => state.todayOrders)
  const { cuadres } = useSelector((state) => state.cuadres)
  const [chartData, setChartData] = useState([])
  const [todayOrders, setTodayOrders] = useState([])
  const [dateStart, setDateStart] = useState(
    new Date().toISOString().split('T')[0]
  )
  const [dateEnd, setDateEnd] = useState(new Date().toISOString().split('T')[0])

  useEffect(() => {
    getChartData()
    getTodayOrders()
  }, [orders])

  useEffect(() => {
    getChartData(true)
    getTodayOrders()
  }, [datesOrders])

  useEffect(() => {
    dispatch(listProducts())
    // dispatch(listOrders())
    dispatch(listTodayOrders())
    dispatch(listTodayCuadres())
    dispatch(listUsers())
    dispatch(listMonthOrders())
  }, [])

  const getChartData = (dates) => {
    const data = []
    let chartItem = {}
    const currentItems = dates ? datesOrders : orders
    products?.forEach((product) => {
      chartItem.name = product.name
      chartItem.qty = 0
      currentItems?.forEach((order) => {
        order.orderItems.forEach((item) => {
          if (item.name === chartItem.name) {
            chartItem.qty = chartItem.qty + Number(item.qty)
          }
        })
      })
      data.push(chartItem)
      chartItem = {}
    })
    setChartData(data)
  }

  let cash = todayOrders.reduce((acc, i) => acc + (i.amount || i.totalPrice), 0)

  const getTodayOrders = () => {
    let arr = []
    orders?.map((order) => {
      if (isToday(new Date(order.createdAt))) arr.push(order)
    })
    setTodayOrders(arr)
  }

  const handleChangeDate = (e) => {
    const value = e.target.value
    if (e.target.name === 'dateStart') {
      setDateStart(value)
      dispatch(listDatesOrders(value, dateEnd))
    } else {
      setDateEnd(value)
      dispatch(listDatesOrders(dateStart, value))
    }
  }

  return (
    <Layout title={'Dashboard'} section={'Dashboard'}>
      {loading ? (
        <Loading />
      ) : (
        <>
          <div className='mt-5 px-8'>
            <h2 className='text-xl font-semibold text-gray-500 mb-5'>Today</h2>
            <div className='grid grid-cols-3 gap-x-10 '>
              <div className='rounded-3xl overflow-hidden shadow-md'>
                <div className='bg-white flex p-8'>
                  <div className='flex-shrink-0 mr-3'>
                    <img
                      className='h-8 w-8'
                      src='https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg'
                      alt='Workflow'
                    />
                  </div>
                  <div>
                    <h4 className=' text-gray-400 font-bold mb-2'>
                      Total Sells
                    </h4>
                    <h2 className='text-4xl text-gray-700'>
                      {todayOrders.length}
                    </h2>
                  </div>
                </div>
              </div>
              <div className='rounded-3xl overflow-hidden shadow-md'>
                <div className='bg-white flex p-8'>
                  <div className='flex-shrink-0 mr-3'>
                    <img
                      className='h-8 w-8'
                      src='https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg'
                      alt='Workflow'
                    />
                  </div>
                  <div>
                    <h4 className=' text-gray-400 font-bold mb-2'>New Users</h4>
                    <h2 className='text-4xl text-gray-700'>18</h2>
                  </div>
                </div>
              </div>
              <div className='rounded-3xl overflow-hidden shadow-md'>
                <div className='bg-white flex p-8'>
                  <div className='flex-shrink-0 mr-3'>
                    <img
                      className='h-8 w-8'
                      src='https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg'
                      alt='Workflow'
                    />
                  </div>
                  <div>
                    <h4 className=' text-gray-400 font-bold mb-2'>Earnings</h4>
                    <h2 className='text-4xl text-gray-700'>
                      ${cash.toFixed(2)}
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <h2 className='text-xl font-semibold text-gray-500 mb-5 px-8 mt-10'>
              Last 30 days
            </h2>
            <div className='px-8'>
              <div className='flex justify-between items-center'>
                <div>
                  <input
                    type='date'
                    value={dateStart}
                    name='dateStart'
                    onChange={handleChangeDate}
                    className='p-1 bg-gray-200 rounded-lg cursor-pointer outline-none mr-3 mb-3'
                  />
                  <input
                    type='date'
                    value={dateEnd}
                    name='dateEnd'
                    onChange={handleChangeDate}
                    className='p-1 bg-gray-200 rounded-lg cursor-pointer outline-none'
                  />
                </div>
                <button
                  onClick={() =>
                    router.push({
                      pathname: '/admin/excelData',
                      query: { orders: JSON.stringify(chartData) },
                    })
                  }
                  className='bg-green-400 hover:bg-green-500 px-8 py-2 rounded-lg'
                >
                  Excel
                </button>
              </div>
              <BarChart chartData={chartData} />
            </div>
          </div>
        </>
      )}
      {/* <div className='px-8 mt-10'>
        <h2 className='text-xl font-semibold text-gray-500 mb-5'>Sells</h2>
        <LineChart />
      </div> */}
      {/* <div className='px-8 mt-10'>
        <h2 className='text-xl font-semibold text-gray-500 mb-5'>Sells</h2>
        <div className='grid grid-cols-2 gap-x-40'>
          <div className='bg-white rounded-3xl shadow-lg'>
            <DoughnutChart />
          </div>
          <div className='bg-white rounded-3xl shadow-lg'>
            <DoughnutChart />
          </div>
        </div>
      </div> */}
    </Layout>
  )
}

export default Home
